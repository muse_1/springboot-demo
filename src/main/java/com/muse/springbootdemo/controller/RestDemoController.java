package com.muse.springbootdemo.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestDemoController {

    @GetMapping("/student")
    public String getStudent() {
        return "---------getStudent---------";
    }

    @PostMapping("/student")
    public String postStudent() {
        return "---------postStudent---------";
    }

    @PutMapping("/student")
    public String putStudent() {
        return "---------putStudent---------";
    }

    @DeleteMapping("/student")
    public String deleteStudent() {
        return "---------deleteStudent---------";
    }
}


