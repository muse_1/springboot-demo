package com.muse.springbootdemo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.muse.springbootdemo.entity.Teacher;

@RestController
public class DemoController {
    /** http://localhost:8888/get/aaa/detail/bbb?name=muse&age=20 */
    @GetMapping("/get/{orderId}/detail/{couponNo}")
    public Map<String, Object> get(@PathVariable("orderId") String orderId,
                                   @PathVariable("couponNo") String couponNo,
                                   @PathVariable Map<String, String> pathVariableMap,
                                   @RequestHeader("Host") String host,
                                   @RequestHeader Map<String, String> headerMap,
                                   @RequestParam("name") String name,
                                   @RequestParam Map<String, String> requestParamMap) {
        Map<String, Object> map = new HashMap<>();
        map.put("orderId", orderId);
        map.put("couponNo", couponNo);
        map.put("pathVariableMap", pathVariableMap);
        map.put("host", host);
        map.put("headerMap", headerMap);
        map.put("name", name);
        map.put("requestParamMap", requestParamMap);
        return map;
    }

    /** http://localhost:8888/teacher   key=teacher value=muse,15,1*/
    @PostMapping("/teacher")
    public String teacher(Teacher teacher) {
        return teacher.toString();
    }
}
