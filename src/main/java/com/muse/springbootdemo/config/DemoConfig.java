package com.muse.springbootdemo.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.muse.springbootdemo.entity.Gun;
import com.muse.springbootdemo.entity.Muse;
import com.muse.springbootdemo.entity.Soldier;
import com.muse.springbootdemo.entity.Teacher;

@ImportResource({"classpath:oldbean.xml"})
@Import(value={Gun.class, Soldier.class}) // 测试@Import
@EnableConfigurationProperties(Muse.class) // 测试引入自定义配置Muse
@Configuration
public class DemoConfig {

    /** 默认来说，方法名称就是beanName */
    @Bean
    public Gun gun() {
        Gun gun = new Gun();
        gun.setName("Gun");
        gun.setBulletNums(20);
        gun.setDesc("我是一把普通的枪");
        return gun;
    }

    /** 也可以通过@Bean("xxx")指定beanName */
    @Bean("ak47")
    public Gun gun1() {
        Gun gun = new Gun();
        gun.setName("AK47");
        gun.setBulletNums(40);
        gun.setDesc("我是AK47，便宜而且威力大");
        return gun;
    }

    /** 测试嵌套赋值 */
    @Bean
    public Soldier soldier() {
        Soldier soldier = new Soldier();
        soldier.setName("muse");
        soldier.setAge(20);
        soldier.setGun(gun()); // 将IOC中的Gun实例赋值给Soldier
        return soldier;
    }

    /** 测试@ConditionalOnBean */
    @Bean("m416")
    public Gun gun2() {
        Gun gun = new Gun();
        gun.setName("M416");
        gun.setBulletNums(60);
        gun.setDesc("我是M416，虽然昂贵但是威力大");
        return gun;
    }
    @Bean
    @ConditionalOnBean(name = "m416")
    public Soldier soldier4War() {
        Soldier soldier = new Soldier();
        soldier.setName("muse");
        soldier.setAge(20);
        soldier.setGun(gun2()); // 将IOC中的Gun实例赋值给Soldier
        return soldier;
    }

    /** 自定义解析 */
    @Bean
    public WebMvcConfigurer webMvcConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addFormatters(FormatterRegistry registry) {
                registry.addConverter(new Converter<String, Teacher>() {
                    @Override
                    public Teacher convert(String source) {
                        if (!StringUtils.hasText(source)) {
                            return null;
                        }
                        String[] sourceArgs = source.split(",");
                        Teacher teacher = new Teacher();
                        teacher.setName(sourceArgs[0]);
                        teacher.setAge(Integer.valueOf(sourceArgs[1]));
                        teacher.setSex(Integer.valueOf(sourceArgs[2]));
                        return teacher;
                    }
                });
            }
        };
    }
}

