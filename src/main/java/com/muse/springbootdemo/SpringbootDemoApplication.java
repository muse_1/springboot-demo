package com.muse.springbootdemo;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.muse.springbootdemo.config.DemoConfig;
import com.muse.springbootdemo.entity.Gun;
import com.muse.springbootdemo.entity.Muse;
import com.muse.springbootdemo.entity.Soldier;

//@ComponentScan(basePackages={"com.muse"})
//@SpringBootApplication

// @SpringBootApplication(scanBasePackages={"com.muse"})

@SpringBootApplication
public class SpringbootDemoApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringbootDemoApplication.class, args);

        /** 获得所有beanName列表 */
        // Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);

        /** 获得自定义配置文件Muse的值 */
        Muse muse = context.getBean(Muse.class);
        System.out.println("muse = " + muse);

        /** 测试@Bean注解 */
        Gun gun = context.getBean("gun", Gun.class);
        System.out.println("gun = " + gun);
        gun = context.getBean("ak47", Gun.class);
        System.out.println("gun = " + gun);

        /** 测试嵌套赋值 */
        Soldier soldier = context.getBean("soldier", Soldier.class);
        System.out.println("soldier = " + soldier);

        /** 测试每次调用gun()是否都是重新创建对象 */
        DemoConfig demoConfig = context.getBean(DemoConfig.class);
        Gun gun1 = demoConfig.gun();
        Gun gun2 = demoConfig.gun();
        Gun gun3 = demoConfig.gun();
        System.out.println("gun1 == gun2 is " + (gun1 == gun2));
        System.out.println("gun1 == gun3 is " + (gun1 == gun3));
        System.out.println("gun2 == gun3 is " + (gun2 == gun3));

        /** 测试@Import */
        boolean exists = context.containsBean("com.muse.springbootdemo.entity.Gun");
        System.out.println("@Import gun exists = " + exists);
        exists = context.containsBean("com.muse.springbootdemo.entity.Soldier");
        System.out.println("@Import soldier exists = " + exists);

        /** 测试@ConditionalOnBean */
        exists = context.containsBean("soldier4War");
        System.out.println("soldier4War exists = " + exists);

        /** 测试@Import */
        exists = context.containsBean("awm");
        System.out.println("awm exists = " + exists);
    }

}
