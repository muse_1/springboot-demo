package com.muse.springbootdemo.entity;

import lombok.Data;

/**
 * @description
 * @author: muse
 **/
@Data
public class Address {
    private String street;
    private String city;
}
