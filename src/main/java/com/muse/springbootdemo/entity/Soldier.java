package com.muse.springbootdemo.entity;

import lombok.Data;
import lombok.ToString;

@Data
public class Soldier {
    private String name;
    private int age;
    private Gun gun;
}
