package com.muse.springbootdemo.entity;

import lombok.Data;

@Data
public class Gun {
    private String name;
    private int bulletNums;
    private String desc;
}
