package com.muse.springbootdemo.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import lombok.ToString;

/**
 * @description
 * @author: muse
 **/
@Data
@ToString
public class Teacher {
    private String name;
    private int age;
    private int sex;
}
