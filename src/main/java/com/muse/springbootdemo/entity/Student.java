package com.muse.springbootdemo.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

/**
 * @description
 * @author: muse
 **/
@ConfigurationProperties(prefix = "student")
@Component
@Data
public class Student {
    private String name;
    private Boolean sex;
    private Date birth;
    private Integer age;
    private Address address;
    private String[] interests;
    private List<String> friends;
    private Map<String, Double> score;
    private Set<Double> weightHis;
    private Map<String, List<Address>> allFriendsAddress;
}
