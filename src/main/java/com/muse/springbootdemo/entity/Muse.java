package com.muse.springbootdemo.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.ToString;

// @Component
@ConfigurationProperties(prefix = "muse", ignoreUnknownFields = true)
@Data
public class Muse {
    private String name;
    private int age;
}
